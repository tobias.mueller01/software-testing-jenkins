package com.softwaretesting.testing.customerManagement.service;

import com.softwaretesting.testing.dao.CustomerRepository;
import com.softwaretesting.testing.exception.BadRequestException;
import com.softwaretesting.testing.exception.CustomerNotFoundException;
import com.softwaretesting.testing.model.Customer;
import com.softwaretesting.testing.validator.CustomerValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class CustomerManagementServiceImpTest {

    @InjectMocks
    private CustomerManagementServiceImp customerManagementService;

    @Mock
    private CustomerRepository customerRepository;

    @Mock
    private CustomerValidator customerValidator;

    @Captor
    ArgumentCaptor<String> stringCaptor1;

    @Captor
    ArgumentCaptor<String> stringCaptor2;

    @Captor
    ArgumentCaptor<Long> idCaptor1;

    @Captor
    ArgumentCaptor<Long> idCaptor2;

    @Captor
    ArgumentCaptor<String> labelCaptor;

    @Captor
    ArgumentCaptor<Customer> customerCaptor;
    @Captor
    ArgumentCaptor<Optional<Customer>> optionalCustomerCaptor;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    private Customer getSampleCustomer() {
        return new Customer(1234L, "arth", "Arthur Dent", "+1234567890");
    }

    private ArrayList<Customer> getSampleCustomerList() {
        ArrayList<Customer> list = new ArrayList<>();
        list.add(getSampleCustomer());
        list.add(new Customer(4321L, "ford", "Ford Prefect", "+9876543210"));
        return list;
    }

    @Test
    void shouldReturnListOfAllCustomers() {
        ArrayList<Customer> testList = getSampleCustomerList();
        when(customerRepository.findAll()).thenReturn(testList);
        Collection<Customer> resultCollection = customerManagementService.list();

        assertEquals(testList.size(), resultCollection.size());
        assertTrue(resultCollection.contains(testList.get(0)));
        assertTrue(resultCollection.contains(testList.get(1)));
    }

    @Test
    void shouldFindExistingCustomerByName() {
        final Customer testCustomer = getSampleCustomer();
        when(customerRepository.findByUserName(stringCaptor1.capture())).thenReturn(Optional.of(testCustomer));
        doNothing().when(customerValidator).validate404(optionalCustomerCaptor.capture(), labelCaptor.capture(), stringCaptor2.capture());
        Customer resultCustomer = customerManagementService.findByUserName(testCustomer.getUserName());

        assertEquals(testCustomer, resultCustomer);
        assertEquals(testCustomer.getUserName(), stringCaptor1.getValue());
        assertEquals(testCustomer.getUserName(), stringCaptor2.getValue());
        assertEquals("User-Name", labelCaptor.getValue());
        assertTrue(optionalCustomerCaptor.getValue().isPresent());
        assertEquals(testCustomer, optionalCustomerCaptor.getValue().get());
    }

    @Test
    void shouldNotFindNonexistentCustomerByName() {
        final String testUsername = "nonexistent_user";
        when(customerRepository.findByUserName(stringCaptor1.capture())).thenReturn(Optional.empty());
        doThrow(ResponseStatusException.class).when(customerValidator).validate404(optionalCustomerCaptor.capture(), labelCaptor.capture(), stringCaptor2.capture());

        assertThrows(ResponseStatusException.class, () -> customerManagementService.findByUserName(testUsername));
        assertEquals(testUsername, stringCaptor1.getValue());
        assertEquals(testUsername, stringCaptor2.getValue());
        assertEquals("User-Name", labelCaptor.getValue());
        assertTrue(optionalCustomerCaptor.getValue().isEmpty());
    }

    @Test
    void shouldFindExistingCustomerById() {
        final Customer testCustomer = getSampleCustomer();
        when(customerRepository.findById(idCaptor1.capture())).thenReturn(Optional.of(testCustomer));
        doNothing().when(customerValidator).validate404(optionalCustomerCaptor.capture(), labelCaptor.capture(), stringCaptor2.capture());
        Customer resultCustomer = customerManagementService.findById(testCustomer.getId());

        assertEquals(testCustomer, resultCustomer);
        assertEquals(testCustomer.getId(), idCaptor1.getValue());
        assertEquals(testCustomer.getId().toString(), stringCaptor2.getValue());
        assertEquals("id", labelCaptor.getValue());
        assertTrue(optionalCustomerCaptor.getValue().isPresent());
        assertEquals(testCustomer, optionalCustomerCaptor.getValue().get());
    }

    @Test
    void shouldNotFindNonexistentCustomerById() {
        final long testId = 9999L;
        when(customerRepository.findById(idCaptor1.capture())).thenReturn(Optional.empty());
        doThrow(ResponseStatusException.class).when(customerValidator).validate404(optionalCustomerCaptor.capture(), labelCaptor.capture(), stringCaptor2.capture());

        assertThrows(ResponseStatusException.class, () -> customerManagementService.findById(testId));
        assertEquals(testId, idCaptor1.getValue().longValue());
        assertEquals(Long.toString(testId), stringCaptor2.getValue());
        assertEquals("id", labelCaptor.getValue());
        assertTrue(optionalCustomerCaptor.getValue().isEmpty());
    }


    @Test
    void shouldFindExistingCustomerByPhoneNumber() {
        final Customer testCustomer = getSampleCustomer();
        when(customerRepository.selectCustomerByPhoneNumber(stringCaptor1.capture())).thenReturn(Optional.of(testCustomer));
        doNothing().when(customerValidator).validate404(optionalCustomerCaptor.capture(), labelCaptor.capture(), stringCaptor2.capture());
        Customer resultCustomer = customerManagementService.selectCustomerByPhoneNumber(testCustomer.getPhoneNumber());

        assertEquals(testCustomer, resultCustomer);
        assertEquals(testCustomer.getPhoneNumber(), stringCaptor1.getValue());
        assertEquals(testCustomer.getPhoneNumber(), stringCaptor2.getValue());
        assertEquals("phone number", labelCaptor.getValue());
        assertTrue(optionalCustomerCaptor.getValue().isPresent());
        assertEquals(testCustomer, optionalCustomerCaptor.getValue().get());
    }

    @Test
    void shouldNotFindNonexistentCustomerByPhoneNumber() {
        final String testPhoneNumber = "+9876543210";
        when(customerRepository.selectCustomerByPhoneNumber(stringCaptor1.capture())).thenReturn(Optional.empty());
        doThrow(ResponseStatusException.class).when(customerValidator).validate404(optionalCustomerCaptor.capture(), labelCaptor.capture(), stringCaptor2.capture());

        assertThrows(ResponseStatusException.class, () -> customerManagementService.selectCustomerByPhoneNumber(testPhoneNumber));
        assertEquals(testPhoneNumber, stringCaptor1.getValue());
        assertEquals(testPhoneNumber, stringCaptor2.getValue());
        assertEquals("phone number", labelCaptor.getValue());
        assertTrue(optionalCustomerCaptor.getValue().isEmpty());
    }

    @Test
    void shouldDeleteExistingCustomer() {
        final long testId = 1234L;
        when(customerRepository.existsById(idCaptor1.capture())).thenReturn(true);
        doNothing().when(customerRepository).deleteById(idCaptor2.capture());
        customerManagementService.delete(testId);

        assertEquals(testId, idCaptor1.getValue().longValue());
        assertEquals(testId, idCaptor2.getValue().longValue());
    }

    @Test
    void shouldNotDeleteNonexistentCustomer() {
        final long testId = 1234L;
        when(customerRepository.existsById(idCaptor1.capture())).thenReturn(false);

        CustomerNotFoundException exception = assertThrows(CustomerNotFoundException.class, () -> customerManagementService.delete(testId));
        assertEquals(testId, idCaptor1.getValue().longValue());
        final String expectedMessage = "Customer with id " + testId + " does not exists";       // is typo intended?
        assertEquals(expectedMessage, exception.getMessage());
        verify(customerRepository, times(0)).deleteById(anyLong());
    }

    @Test
    void shouldAddNewCustomer() {
        final Customer testCustomer = getSampleCustomer();
        when(customerRepository.selectCustomerByPhoneNumber(stringCaptor1.capture())).thenReturn(Optional.empty());
        when(customerRepository.save(customerCaptor.capture())).thenReturn(testCustomer);
        Customer resultCustomer = customerManagementService.addCustomer(testCustomer);

        assertEquals(testCustomer, resultCustomer);
        assertEquals(testCustomer.getPhoneNumber(), stringCaptor1.getValue());
        assertEquals(testCustomer, customerCaptor.getValue());
    }

    @Test
    void shouldThrowExceptionWhenAddingCustomerWithExistingPhoneNumber() {
        final Customer testCustomer = getSampleCustomer();
        when(customerRepository.selectCustomerByPhoneNumber(stringCaptor1.capture())).thenReturn(Optional.of(testCustomer));

        BadRequestException exception = assertThrows(BadRequestException.class, () -> customerManagementService.addCustomer(testCustomer));
        final String expectedMessage = "Phone Number " + testCustomer.getPhoneNumber() + " taken";
        assertEquals(expectedMessage, exception.getMessage());
        verify(customerRepository, times(0)).save(any(Customer.class));
    }

    @Test
    void shouldAddCustomerCollection() {
        ArrayList<Customer> testList = getSampleCustomerList();
        when(customerRepository.saveAll(anyIterable())).thenReturn(testList);
        Collection<Customer> resultCollection = customerManagementService.saveAll(testList);

        assertEquals(testList.size(), resultCollection.size());
        assertTrue(resultCollection.contains(testList.get(0)));
        assertTrue(resultCollection.contains(testList.get(1)));
    }

    // TODO: What could go wrong with saveAll()?

}