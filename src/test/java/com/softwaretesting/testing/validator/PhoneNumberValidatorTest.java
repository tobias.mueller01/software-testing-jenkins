package com.softwaretesting.testing.validator;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PhoneNumberValidatorTest {

    private final PhoneNumberValidator validator = new PhoneNumberValidator();

    // ========================================== Positive Test Cases ==========================================
    @Test
    void itShouldAcceptTypicalNumber() {
        assertTrue(validator.validate("+495513923366"));    // studIT support hotline
    }

    @Test
    void itShouldAcceptMinLength() {
        // Note: It is not clear how long the minimal possible phone number should be. The shortest country code
        // is +1 and the only limit I could find while glancing over the standard are a maximum of 15 digits in total.
        // One source [1] says the shortest number in use has seven digits, but let's keep it simple again...
        //
        // [1] https://www.oreilly.com/library/view/regular-expressions-cookbook/9781449327453/ch04s03.html

        assertTrue(validator.validate("+12"));
    }

    @Test
    void itShouldAcceptMaxLength() {
        assertTrue(validator.validate("+491234567890123"));
    }

    @Test
    void itShouldAcceptSpacesAsSeparators() {
        assertTrue(validator.validate("+49 551 39 23366"));    // studIT support hotline
    }


    // ========================================== Negative Test Cases ==========================================
    @Test
    void itShouldRejectNull() {
        assertFalse(validator.validate(null));
    }

    @Test
    void itShouldRejectEmptyString() {
        assertFalse(validator.validate(""));
    }

    @Test
    void itShouldRejectMissingPlusSign() {
        assertFalse(validator.validate("495513912345"));
    }

    @Test
    void itShouldRejectOnlyOneDigit() {
        assertFalse(validator.validate("+1"));
    }

    @Test
    void itShouldRejectMoreThanFifteenDigits() {
        assertFalse(validator.validate("+1234567890123456"));
    }

    @Test
    void itShouldRejectCountryCodeStartingWithZero() {
        assertFalse(validator.validate("+012345"));
    }

    @Test
    void itShouldRejectInvalidCharacters() {
        assertFalse(validator.validate("+4955139-12345"));
    }

}