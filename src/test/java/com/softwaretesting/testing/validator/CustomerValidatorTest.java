package com.softwaretesting.testing.validator;

import com.softwaretesting.testing.model.Customer;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class CustomerValidatorTest {

    private final CustomerValidator validator = new CustomerValidator();

    /**
     * Creates an Optional object containing a customer object for test purposes
     * @return Optional with test customer object
     */
    private Optional<Customer> getOptionalCustomer() {
        Customer customer = new Customer(0L, "testuser", "Test User", "+49111222333");
        return Optional.of(customer);
    }

    @Test
    void shouldNotThrowErrorWhenObjectPresent() {
        Optional<Customer> optionalCustomer = getOptionalCustomer();
        assertDoesNotThrow(() -> validator.validate404(optionalCustomer, "Test Label", "Test Value"));
    }

    @Test
    void shouldThrowErrorWhenObjectNotPresent() {
        String expectedMessage = "404 NOT_FOUND \"java.util.Optional with Test Label'Test Value' does not exist.\"";
        Optional<Customer> emptyOptional = Optional.empty();
        ResponseStatusException exception =  assertThrows(ResponseStatusException.class, () -> validator.validate404(emptyOptional, "Test Label", "Test Value"));
        assertEquals(expectedMessage, exception.getMessage());
        assertEquals(HttpStatus.NOT_FOUND, exception.getStatus());
    }

    @Test
    void shouldThrowNullPointerExceptionNullObject() {
        assertThrows(NullPointerException.class, () -> validator.validate404(null, "Test Label", "Test Value"));
    }
}