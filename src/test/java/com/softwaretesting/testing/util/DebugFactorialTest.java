package com.softwaretesting.testing.util;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.junit.jupiter.api.Assertions.*;

class DebugFactorialTest {

    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    @BeforeEach
    public void setUp() {
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @AfterEach
    public void tearDown() {
        System.setOut(System.out);
    }

    @Test
    void shouldConstructClass() {
        // Even if it does not make sense to instantiate a class with only static members...
        DebugFactorial debugFactorial = new DebugFactorial();
        assertTrue(debugFactorial instanceof DebugFactorial);
    }


    @ParameterizedTest
    @CsvSource({
            "-1,    1",
            "0,     1",
            "1,     1",
            "2,     2",
            "4,    24"
    })
    void shouldCalculateFactorialOfValidNumbers(int operand, int result) {
        assertEquals(result, DebugFactorial.factorial(operand));
    }

    // The factorial of a negative number is not defined, the implementation
    // instead returns 1.
    @Disabled("In mathematics, factorials of negative numbers are undefined. There is, however, no requirements document.")
    @Test
    void shouldThrowExceptionForFactorialOfNegativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> DebugFactorial.factorial(-1));
    }

    @Test
    void shouldOutputTextWithFactorialOfFive() {
        String[] args = {"DebugFactorial"};
        DebugFactorial.main(args);
        assertEquals("Factorial of 5 is 120", outputStreamCaptor.toString().trim());
    }

}