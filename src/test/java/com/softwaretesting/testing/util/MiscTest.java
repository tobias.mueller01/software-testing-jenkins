package com.softwaretesting.testing.util;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.*;

class MiscTest {

    @Test
    void shouldConstructClass() {
        // Even if it does not make sense to instantiate a class with only static members...
        Misc misc = new Misc();
        assertTrue(misc instanceof Misc);
    }

    @Test
    void shouldAddTwoNumbers() {
        assertEquals(42, Misc.sum(40, 2));
    }

    @Test
    void shouldDivideTwoIntegersWithoutRemainder() {
        assertEquals(5, Misc.divide(10, 2));
    }

    // Based on the method signature (return type of double) I would expect a correct floating-point
    // division to take place. Instead, an integer division is performed with the result being implicitly
    // cast to double. Since we have no specification, however, this behavior could be perfectly valid.
    // I have disabled this test case, because JaCoCo would not generate a report with failing test cases.
    @Disabled("Due to a return type of `double`, floating point division should be expected. There is, however, no requirements document.")
    @Test
    void shouldDivideTwoIntegersWithRemainder() {
        assertEquals(4.5, Misc.divide(9, 2));
    }

    @Test
    void shouldThrowRuntimeExceptionWhenDividingByZero() {
        String expectedMessage = "This operation would result in division by zero error.";
        RuntimeException exception = assertThrows(RuntimeException.class, () -> Misc.divide(1,0));
        assertEquals(expectedMessage, exception.getMessage());
    }

    @Test
    void shouldAcceptSupportedColor() {
        assertTrue(Misc.isColorSupported(Misc.Color.YELLOW));
    }

    @Test
    void shouldNotAcceptNullColor() {
        String expectedMessage = "color cannot be null";
        IllegalArgumentException exception = assertThrows(IllegalArgumentException.class, () -> Misc.isColorSupported(null));
        assertEquals(expectedMessage, exception.getMessage());
    }

    // No working test case for false branch possible
    //    @Test
    //    public void shouldNotAcceptInvalidColor() {
    //        assertFalse(Misc.isColorSupported( (Misc.Color) Integer.valueOf(42)));
    //    }

    @ParameterizedTest
    @CsvSource({
            "-1,    1",
            "0,     1",
            "1,     1",
            "2,     2",
            "4,    24"
    })
    void shouldCalculateFactorialOfValidNumbers(int operand, int result) {
        assertEquals(result, Misc.calculateFactorial(operand));
    }

    // The factorial of a negative number is not defined, the implementation
    // instead returns 1.
    @Disabled("In mathematics, factorials of negative numbers are undefined. There is, however, no requirements document.")
    @Test
    void shouldThrowExceptionForFactorialOfNegativeNumber() {
        assertThrows(IllegalArgumentException.class, () -> Misc.calculateFactorial(-1));
    }

    @ParameterizedTest
    @ValueSource(ints = {2, 3, 23, 29, 31, 37, 1021})
    void shouldReturnTrueForPrimeNumbers(int number) {
        int START_VALUE = 2;
        assertTrue(Misc.isPrime(number, START_VALUE));
    }

    @ParameterizedTest
    @ValueSource(ints = {1, 4, 9, 21, 69, 1337})
    void shouldReturnFalseForNonPrimeNumbers(int number) {
        int START_VALUE = 2;
        assertFalse(Misc.isPrime(number, START_VALUE));
    }

    @Test
    void shouldReturnTwoIsEven() {
        assertTrue(Misc.isEven(2));
    }

    @Test
    void shouldReturnOneIsNotEven() {
        assertFalse(Misc.isEven(1));
    }


}