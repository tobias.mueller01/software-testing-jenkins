package com.softwaretesting.testing.customerRegistration.service;

import com.softwaretesting.testing.dao.CustomerRepository;
import com.softwaretesting.testing.exception.BadRequestException;
import com.softwaretesting.testing.model.Customer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

class CustomerRegistrationServiceTest {

    @InjectMocks
    private CustomerRegistrationService customerRegistrationService;

    @Mock
    private CustomerRepository customerRepository;

    @Captor
    ArgumentCaptor<String> argumentCaptor;

    @BeforeEach
    void setUp(){
        MockitoAnnotations.initMocks(this);
    }

    /**
     * Creates an Optional object containing a customer object for test purposes
     * @return Optional with test customer object
     */
    private Customer getSampleCustomer() {
        return new Customer(1234L, "arth", "Arthur Dent", "+1234567890");
    }

    @Test
    void shouldSaveNewCustomer() {
        Customer testCustomer = getSampleCustomer();

        when(customerRepository.selectCustomerByPhoneNumber(argumentCaptor.capture())).thenReturn(Optional.empty());
        when(customerRepository.save(any(Customer.class))).thenReturn(testCustomer);
        Customer resultCustomer = customerRegistrationService.registerNewCustomer(testCustomer);

        assertEquals(testCustomer.getPhoneNumber(), argumentCaptor.getValue());
        assertEquals(resultCustomer, testCustomer);
    }

    @Test
    void shouldThrowIllegalStateExceptionWhenAlreadyRegistered() {
        Customer testCustomer = getSampleCustomer();

        when(customerRepository.selectCustomerByPhoneNumber(argumentCaptor.capture())).thenReturn(Optional.of(testCustomer));

        IllegalStateException exception = assertThrows(IllegalStateException.class, () -> customerRegistrationService.registerNewCustomer(testCustomer));

        assertEquals(testCustomer.getPhoneNumber(), argumentCaptor.getValue());
        assertEquals("You are already registered", exception.getMessage());
    }

    @Test
    void shouldThrowBadRequestExceptionIfNumberTaken() {
        Customer existingCustomer = getSampleCustomer();
        Customer newCustomer = new Customer(4321L, "ford", "Ford Prefect", existingCustomer.getPhoneNumber());

        when(customerRepository.selectCustomerByPhoneNumber(argumentCaptor.capture())).thenReturn(Optional.of(existingCustomer));

        BadRequestException exception = assertThrows(BadRequestException.class, () -> customerRegistrationService.registerNewCustomer(newCustomer));

        assertEquals(newCustomer.getPhoneNumber(), argumentCaptor.getValue());
        assertEquals("Phone Number " + existingCustomer.getPhoneNumber() + " taken", exception.getMessage());
    }


}