package com.softwaretesting.testing.dao;

import com.softwaretesting.testing.model.Customer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

// Adapted annotations from https://howtodoinjava.com/spring-boot2/testing/spring-boot-2-junit-5/
@ExtendWith(SpringExtension.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class CustomerRepositoryTest {

    @Autowired
    private CustomerRepository customerRepository;

    @BeforeEach
    void setUp() {
        Customer customer1 = new Customer(1234L, "arth", "Arthur Dent", "+1234567890");
        Customer customer2 = new Customer(4321L, "ford", "Ford Prefect", "+9876543210");
        Customer customer3 = new Customer(2143L, "zaph", "Zaphod Beeblebrox", "+111222333");
        customerRepository.save(customer1);
        customerRepository.save(customer2);
        customerRepository.save(customer3);
    }

    @AfterEach
    void tearDown() {
        customerRepository.deleteAll();
    }

    private Customer createOneTestCustomer() {
        return new Customer(6666L, "genghis_khan", "Mr L Prosser", "+666777888");
    }

    // ========================================== Positive Test Cases ==========================================
    // ---- Test cases w/o interdependencies ---
//    @Test
//    public void itShouldFindByIdExistingRecord() {
//        Optional<Customer> result = customerRepository.findById(1234L);
//        assertTrue(result.isPresent());
//        assertEquals("arth", result.get().getUserName());
//    }

    @Test
    void itShouldNotFindByIdNonexistingRecord() {
        Optional<Customer> result = customerRepository.findById(4242L);
        assertTrue(result.isEmpty());
    }

//    @Test
//    public void itShouldReturnTrueExistsById() {
//        assertTrue(customerRepository.existsById(1234L));
//    }

    @Test
    void itShouldReturnFalseExistsById() {
        assertFalse(customerRepository.existsById(4242L));
    }

    @Test
    void itShouldFindByUserNameExistingRecord() {
        Optional<Customer> result = customerRepository.findByUserName("ford");
        assertTrue(result.isPresent());
        assertEquals("Ford Prefect", result.get().getName());
    }

    @Test
    void itShouldNotFindByUserNameNonexistingRecord() {
        Optional<Customer> result = customerRepository.findByUserName("trillian");
        assertTrue(result.isEmpty());
    }

    @Test
    void itShouldSelectByPhoneNumberExistingRecord() {
        Optional<Customer> result = customerRepository.selectCustomerByPhoneNumber("+1234567890");
        assertTrue(result.isPresent());
        assertEquals("arth", result.get().getUserName());
    }

    @Test
    void itShouldNotSelectByPhoneNumberNonexistingRecord() {
        Optional<Customer> result = customerRepository.selectCustomerByPhoneNumber("+999999999");
        assertTrue(result.isEmpty());
    }

    @Test
    void itShouldCount() {
        assertEquals(3L, customerRepository.count());
    }

    @Test
    void itShouldDeleteAll() {
        assertEquals(3L, customerRepository.count());
        customerRepository.deleteAll();
        assertEquals(0L, customerRepository.count());
    }

    // ---- Test cases w/ interdependencies ---
    @Test
    void itShouldSaveNewCustomer() {
        long count_before = customerRepository.count();

        Customer newCustomer = createOneTestCustomer();
        customerRepository.save(newCustomer);

        assertEquals(count_before+1, customerRepository.count());
        Optional<Customer> result = customerRepository.findByUserName(newCustomer.getUserName());
        assertTrue(result.isPresent());
        assertEquals(newCustomer.getName(), result.get().getName());
    }

    @Test void itShouldDeleteCustomer() {
        long count_before = customerRepository.count();

        // Get customer object to delete
        Optional<Customer> result = customerRepository.findByUserName("ford");
        result.ifPresent(customer -> customerRepository.delete(customer));

        assertEquals(count_before-1, customerRepository.count());
        Optional<Customer> new_result = customerRepository.findByUserName("ford");
        assertTrue(new_result.isEmpty());
    }

    // ========================================== Negative Test Cases ==========================================
    @Test
    void itShouldNotFindByUserNameNull() {
        Optional<Customer> result = customerRepository.findByUserName(null);
        assertTrue(result.isEmpty());
    }

    @Test
    void itShouldNotFindByUserNameEmptyString() {
        Optional<Customer> result = customerRepository.findByUserName("");
        assertTrue(result.isEmpty());
    }

    @Test
    void itShouldNotSelectByPhoneNumberNull() {
        Optional<Customer> result = customerRepository.selectCustomerByPhoneNumber(null);
        assertTrue(result.isEmpty());
    }

    @Test
    void itShouldNotSelectByPhoneNumberEmptyString() {
        Optional<Customer> result = customerRepository.selectCustomerByPhoneNumber("");
        assertTrue(result.isEmpty());
    }

}