package com.softwaretesting.testing.validator;

import org.springframework.stereotype.Service;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to check phone numbers for validity
 */
@Service
public class PhoneNumberValidator {

    // Following the E.164 standard (simplified)
    // For more information, see https://en.m.wikipedia.org/wiki/E.164
    private final Pattern internationalPhoneNumberRegex = Pattern.compile("^\\+[1-9][0-9]{1,14}$");

    // Note: Not all values that are accepted by the first part of the Regex are actually assigned as country codes
    // by the ITU. For more elaborate validation, a list of all possible values [1] would have to be included. For the
    // sake of brevity, I take a shortcut here.
    //
    // [1] https://en.wikipedia.org/wiki/List_of_country_calling_codes

    /** Validate a phone number.
     * @param phoneNumber phone number as String
     * @return true if phone number is valid, else false
     */
    public boolean validate(String phoneNumber) {
        if (phoneNumber == null) {
            return false;
        }

        // Spaces as visual separators are allowed but ignored for validation
        String phoneNumberNoSpaces = phoneNumber.replace(" ", "");

        Matcher matcher = internationalPhoneNumberRegex.matcher(phoneNumberNoSpaces);
        return matcher.find();
    }


}
